from tails.app import app


async def test_rendered_number_of_stores_is_correct(aiohttp_client, loop):
    client = await aiohttp_client(app)
    resp = await client.get("/")
    assert resp.status == 200
    page = await resp.text()
    assert page.count('class="card"') == 95
