from tails import stores

# This was chosen as it's close to Alton.
MAGIC_POINT = (51.136975, -0.989996)
MAGIC_POSTCODE = "GU34 1ST"


async def test_load_stores(loop):
    all_stores = await stores.load_stores()
    assert len(all_stores) == 95
    first = next(iter(all_stores))
    assert all_stores[first].name is not None
    assert all_stores[first].postcode == first
    assert all_stores[first].latitude is None
    assert all_stores[first].longitude is None


async def test_populate_stores(loop):
    tails_pc = "TW9 1BN"
    queen_pc = "SW1A 1AA"
    junk_pc = "123456"
    test_stores = {
        tails_pc: stores.Store(name="Tails", postcode=tails_pc),
        queen_pc: stores.Store(name="HM Queen", postcode=queen_pc),
        junk_pc: stores.Store(name="junk", postcode=junk_pc),
    }

    results = await stores.locate_stores(test_stores)
    assert results[0].name == test_stores[queen_pc].name
    assert results[1].name == test_stores[tails_pc].name
    assert results[2].name == test_stores[junk_pc].name

    assert results[0].latitude == 51.501009
    assert results[0].longitude == -0.141588

    assert results[2].latitude is None
    assert results[2].longitude is None


async def test_fast_narrow_stores_narrows_correctly(loop):
    strs = await stores.load_and_locate_stores()
    narrowed = stores.fast_narrow_stores(strs, MAGIC_POINT, 5)

    assert len(narrowed) == 1
    assert narrowed[0].name == "Alton"


async def test_fast_narrow_stores_sorts_from_north_to_south(loop):
    strs = await stores.load_and_locate_stores()
    narrowed = stores.fast_narrow_stores(strs, MAGIC_POINT, 20)

    assert len(narrowed) == 4

    # These are around Alton, north to south.
    expected = [
        "Farnborough",
        "Basingstoke",
        "Farnham",
        "Alton",
    ]
    assert [s.name for s in narrowed] == expected


async def test_slow_narrow_stores_narrows_correctly(loop):
    strs = await stores.load_and_locate_stores()
    narrowed = stores.slow_narrow_stores(strs, MAGIC_POINT, 5)

    assert len(narrowed) == 1
    assert narrowed[0].name == "Alton"


async def test_slow_narrow_stores_sorts_from_north_to_south(loop):
    strs = await stores.load_and_locate_stores()
    narrowed = stores.slow_narrow_stores(strs, MAGIC_POINT, 20)

    # Unlike the fast search, we're expecting only 3 results here. ACCURACY!!
    assert len(narrowed) == 3

    from pprint import pprint

    pprint(narrowed)

    # These are around Alton, north to south.
    expected = [
        "Basingstoke",
        "Farnham",
        "Alton",
    ]
    assert [s.name for s in narrowed] == expected


async def test_narrow_to_postcode(loop):
    strs = await stores.load_and_locate_stores()
    narrowed = await stores.narrow_stores_postcode(strs, MAGIC_POSTCODE, 20)

    assert len(narrowed) == 4

    # These are around Alton, north to south.
    expected = [
        "Farnborough",
        "Basingstoke",
        "Farnham",
        "Alton",
    ]
    assert [s.name for s in narrowed] == expected
