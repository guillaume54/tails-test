import logging

from aiohttp import web
from tails.app import app

logging.basicConfig(level=logging.DEBUG)

web.run_app(app)
