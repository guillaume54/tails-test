TAILS TECHNICAL TEST
====================

This is a small website displaying a list of stores at a given location.

## Initial setup

You must have at least python 3.7 installed.

Start by installing dependencies. Using a virtual environment is recommended.

```
pip install -r requirements/prod.txt
```

## Running the site in prod mode

Just start the `serve.py` script like so:

```
python serve.py
```

Since `aiohttp` is fully asynchronous, a supervisor isn't necessary.

Then open a a browser to `http://localhost:8080`.

## Running the docker website

You need Docker installed. You will need to build an image of the website
and then run it like so:

```
docker build -t tails .
docker run -p 8080:8080 -t tails
```

Then open a a browser to `http://localhost:8080`.

## Running the website in development mode

Install the development dependencies and run the development server:

```
pip install -r requirements/dev.txt
adev runserver tails
```

Then open a a browser to `http://localhost:8000`. Your web-browser will
auto-refresh when a change is made to the code.

## Running unit tests

You will need the development dependencies installed first.

```
python -m pytest tests
```

## Radius search

The radius search can be done in different ways.

The most accurate would be to calculate the distance of each known location
from the provided postcode, and then filter those that are within range. This
would allow accounting for earth's curvature and be a truly circular area.

An approximation is to calculate the min and max latitude and longitude around
the provided center. Of course this results in a square area rather than a
circle. It is however much easier to implement and faster to run. For small
enough areas and depending on business needs, this can be a good enough
solution.

I'll use the square approximation as it's simpler to work with and more
performant.

Since I don't want to mess up implementing the haversine formula to perform
geographical calculations -- and this isn't a maths test -- I'll use the geopy
library for this purpose.

## Cache everywhere

I added some super-simple caching to postcodes.io and the list of stores. That
way the postcodes API doesn't get requests all the time, and we don't load the 
same file again and again.

In both cases the cache needs to be primed, so the very first request is quite
slow. Subsequent requests run at about 7ms for me.

## End

I ended up implementing both models and left them in, but defaulted the postcode
search to fast search. Both are unit tested and work correctly. This concludes
the test.

Using a geographically inclined database like postgres and postgis would make
location searches a lot faster also than doing it in python.
