from dataclasses import dataclass
import aiohttp
import aiofiles
import json
import os
from geopy.distance import geodesic
from geopy import distance

from .postcodes import get_postcodes, get_postcode_coords

STORES_FILE = os.path.join(os.path.dirname(__file__), "stores.json")


stores_cache = None


@dataclass
class Store:
    name: str
    postcode: str
    latitude: float = None
    longitude: float = None
    distance: float = None

    def coords(self):
        return (self.latitude, self.longitude)

    def distance_from(self, origin):
        self.distance = distance.distance(origin, self.coords()).km
        return self.distance


async def load_stores():
    """
    Loads stores from the storage.

    Note: This is weak to duplicated postcodes. Thankfully there are none
    in the hard-coded data store.

    :returns: a dictionary of stores per postcode
    """
    async with aiofiles.open(STORES_FILE, "r") as stores_file:
        records = json.loads(await stores_file.read())
    return {record["postcode"]: Store(**record) for record in records}


async def locate_stores(stores):
    """
    Populate store coordinates.

    :returns: a list of Stores sorted by name ascendingly.
    """
    async with aiohttp.ClientSession() as session:
        pc_infos = await get_postcodes(session, list(stores.keys()))

    for pc in pc_infos:
        stores[pc].latitude = pc_infos[pc][0]
        stores[pc].longitude = pc_infos[pc][1]

    return sorted(stores.values(), key=lambda s: s.name)


async def load_and_locate_stores():
    """
    Load our official list of stores, with coordinates. This function is
    cached.

    :return: a list of Stores sorted by name ascendingly
    """
    global stores_cache
    if not stores_cache:
        stores = await load_stores()
        stores_cache = await locate_stores(stores)
    return stores_cache


class SearchBox:
    """
    A very naive square geographical search box.
    """

    def __init__(self, north, south, east, west):
        """
        All points are coordinates as tuples of floats
        """
        self.min_lat = south.latitude
        self.max_lat = north.latitude
        self.min_lon = west.longitude
        self.max_lon = east.longitude

    def has_store(self, store):
        """
        Checks if store is contained within the search box.
        """
        if store.latitude is None or store.longitude is None:
            return False
        has_lat = self.min_lat < store.latitude < self.max_lat
        has_lon = self.min_lon < store.longitude < self.max_lon

        return has_lat and has_lon


def fast_narrow_stores(stores, origin, radius):
    """
    Filter the list of stores to those within the radius of origin. Uses a
    square box approximation.

    :param stores: a list of Stores
    :param origin: a tuple of latitude, longitude
    :param radius: the search radius in KM (no obsolete imperial units)
    :returns: a new list of Stores
    """
    # Calculate min/max latitude and min/max longitude
    model = geodesic()
    box = SearchBox(
        north=model.destination(origin, bearing=0, distance=radius),
        south=model.destination(origin, bearing=180, distance=radius),
        east=model.destination(origin, bearing=90, distance=radius),
        west=model.destination(origin, bearing=270, distance=radius),
    )

    narrowed_stores = [store for store in stores if box.has_store(store)]
    return sorted(narrowed_stores, key=lambda s: -s.latitude)


def slow_narrow_stores(stores, origin, radius):
    """
    Filter the list of stores to those within the radius of origin. Gives
    accurate results but is slow.

    :param stores: a list of Stores
    :param origin: a tuple of latitude, longitude
    :param radius: the search radius in KM (no obsolete imperial units)
    :returns: a new list of Stores
    """
    # Calculate min/max latitude and min/max longitude
    narrowed_stores = [
        store for store in stores if store.distance_from(origin) < radius
    ]
    return sorted(narrowed_stores, key=lambda s: -s.latitude)


async def narrow_stores_postcode(stores, postcode, radius):
    """
    Filter the list of stores to those within the radius of a postcode.

    :param stores: a list of Stores
    :param postcode: a postcode as a string
    :param radius: the search radius in KM
    :returns: a new list of Stores
    :raises: ValueError if the postcode can't be located
    """
    async with aiohttp.ClientSession() as session:
        postcode_coords = await get_postcode_coords(session, postcode)

    if postcode_coords[0] is None or postcode_coords[1] is None:
        raise ValueError

    return fast_narrow_stores(stores, postcode_coords, radius)
