import logging

logger = logging.Logger(__name__)

API_URL = "https://api.postcodes.io/postcodes"


known_postcodes = {}


async def get_postcode_coords(session, postcode):
    """
    Retrieve coordinates for a single postcode

    :param session: The aiohttp client session
    :param postcode: The postcode to query
    :returns: coordinates as a tuple of floats
    """
    if postcode in known_postcodes:
        logger.info("Return cached location of %s", postcode)
        return known_postcodes[postcode]

    logger.info("Querying location of postcode %s", postcode)
    async with session.get(f"{API_URL}/{postcode}") as resp:
        if resp.status != 200:
            return (None, None)
        data = await resp.json()
        return (data["result"]["latitude"], data["result"]["longitude"])


async def get_postcodes(session, postcodes):
    """
    Retrieve a bunch of postcodes.

    :param session: The aiohttp client session
    :param postcodes: A list of postcodes to query
    :returns: the resolved dict of postcode queries::
        {
            "AA1 1AA": (0.123, 0.123),
        }
    """
    coords = {}
    unknown = []
    for postcode in postcodes:
        if postcode in known_postcodes:
            coords[postcode] = known_postcodes[postcode]
        else:
            unknown.append(postcode)
    logger.debug("Using cache to postcode locations %s", list(coords.keys()))
    logger.debug("Querying postcodes locations for %s", unknown)

    async with session.post(API_URL, json={"postcodes": unknown}) as resp:
        info = await resp.json()
        for pc_info in info["result"]:
            if pc_info["result"] is None:
                coords[pc_info["query"]] = (None, None)
                continue
            pc_coords = (
                pc_info["result"]["latitude"],
                pc_info["result"]["longitude"],
            )
            coords[pc_info["query"]] = pc_coords
            known_postcodes[pc_info["query"]] = pc_coords
    return coords
