import os

from aiohttp import web
import jinja2
import aiohttp_jinja2

from .stores import load_and_locate_stores


async def stores(request):
    stores = await load_and_locate_stores()

    return aiohttp_jinja2.render_template(
        "stores.html", request, context={"stores": stores}
    )


app = web.Application()
aiohttp_jinja2.setup(
    app,
    loader=jinja2.FileSystemLoader(
        os.path.join(os.path.dirname(__file__), "templates")
    ),
)
app.add_routes([web.get("/", stores)])

if __name__ == "__main__":
    print("Use the `serve.py` script instead!")
