FROM python:3.8

EXPOSE 8080

RUN mkdir -p /srv/tails
WORKDIR /srv/tails

COPY requirements /srv/tails/requirements
RUN pip install -r requirements/prod.txt

COPY . /srv/tails/

CMD python serve.py
